/********************************************************

 DISPATCH - A Task Dispatcher.

 A program to manage multiple tasks via a ready queue
 and multiple delays via a delay queue. Runs one task at
 a time. Tasks coming off the delay queue are put on the
 ready queue. Delay queue is ticked from timer interrupt.
 (All status calls removed).

 Ron Kreymborg
 22-Aug-99
    ����������:
init
  InitMulti();
  Dispatch();
  DecrementDelay(); ������� �������, ���� ������ � ����������, �������� �� main
use:
  ReRunMe(0); - ���� ����� ��������� ��� ��� ��� ��������
  ReRunMe(x); - ���� ����� ��������� ��� ��� ����� ����� �
  QueueTask(task); �������� ������
  QueueDelay(task,  x); - �������� ������ task ����� ����� x

********************************************************/

#include    "multi.h"

static void DoQueueTask(void);
static void DoQueueDelay(void);
static void RunTask(void);
static unsigned int GetNewTask(void (*TaskPtr)());
volatile void DelTask(void (*TaskPtr)());
//char dummyu[14];

static struct{
    unsigned int    delay;      // delay in ticks
    void            (*pntr)();  // pointer to task
    unsigned char   next;           // linked list
    unsigned char   del;        //mark for delete
    char            emailBox[11];
    }task[TOTALTASKS + 1];

static unsigned char RunningTask;
static unsigned char TaskHead, TailTask;
static unsigned char NewTask;
static unsigned char *DelayHead;
static unsigned int NewDelay;
static void (*Tpntr)();


//**********************************************************
// Initialise the data structure. Must be called before
// first use.

void InitMulti(void) {
    int i;

    for (i = 0; i < TOTALTASKS; i++){
        task[i].delay = 0x5555;
    }
    DelayHead = &task[0].next;
}


//**********************************************************
// Put a task on the tail of the ready list. Call with
// a pointer to the task as argument.
// Returns TRUE if the task could not be queued.

unsigned int QueueTask(void (*TaskPtr)()) {

    if (GetNewTask(TaskPtr))   // ���� ��������� �������, ���������� ����� � NewTask[1..10]
        return 1;
    Tpntr = TaskPtr;            // set task pointer
    DoQueueTask();              // put new task on queue
    return 0;                   // return ok
}


//**********************************************************
// Put a task in the delay queue such that it will come
// off after <delay> clock ticks. Call with a pointer
// to the task as argument.
// Returns TRUE if the task could not be queued.

unsigned int QueueDelay(void (*pntr)(), unsigned int delay) {

    if (GetNewTask(pntr))   // set NewTask
        return 1;
    Tpntr = pntr;               // set task pointer
    NewDelay = delay;           // set NewDelay
    if (NewDelay)
        DoQueueDelay();         // put task on delay queue
    else
        DoQueueTask();          // if zero just put on ready queue
    return 0;                   // return ok
}


//**********************************************************
// Run the task at the head of the ready queue, or just
// return.

void Dispatch(void) {

    if (TaskHead) {
        RunningTask = TaskHead;
        TaskHead = task[RunningTask].next;  // head is now next
        if (TaskHead == 0)                      // ensure tail is correct
            TailTask = 0;
        RunTask();                                  // run the task
        }
}


//**********************************************************
// Called by the running task just before exit to ensure the
// task is re-run. If <delay> is non-zero, then the task 
// will be re-run after the delay.

void ReRunMe(unsigned int delay) {

    NewDelay = delay;               // set NewDelay
    NewTask = RunningTask;      // set NewTask
}


//**********************************************************
// Call from the main program loop when the task timer 
// interrupt has set its flag. Decrements the delay of the 
// task at the head of the delay queue. If it goes to zero 
// takes it off the delay queue and puts it on the ready 
// queue. Continues to do this until a non-zero delay 
// is found.

void DecrementDelay(void) {

    if (*DelayHead) {
        task[*DelayHead].delay--;                   // decrement head delay
        while (*DelayHead && task[*DelayHead].delay == 0) {
            NewTask = *DelayHead;                   // set NewTask
            *DelayHead = task[*DelayHead].next; // set head to next
            Tpntr = task[NewTask].pntr;             // set task pointer
            DoQueueTask();                              // copy to ready list
            }
        }
}



//**********************************************************
// Private functions begin.
//**********************************************************

// Take the task pointer <Tpntr> and insert it in the data
// structure at <NewTask>. Link from the tail of the queue
// to the <NewTask> position. Ensure the head and tail pointers
// are correct. Set <next> to null.

static void DoQueueTask() {
 
    // Is there a tail task?
    if (TailTask != 0)
        task[TailTask].next = NewTask;  // link tail to new
    else
        TaskHead = NewTask;                 // else new is head
    TailTask = NewTask;                     // tail always new
    task[NewTask].next = 0;                 // tail link always null
    task[NewTask].delay = 0;                // not really required
    task[NewTask].pntr = Tpntr;         // insert task pointer
    NewTask = 0;
}


//**********************************************************
// Take the task pointer <Tpntr> and insert it in the data
// structure at the correct point in the delay queue such
// that the delay for this queue position equals the sum of
// all earlier delays. Ensure the links to and from this
// queue position use the <NewTask> list position.

static void DoQueueDelay() {
    register unsigned char Pntr0, Pntr1;
    register int OldDelVal, DelVal;
 
    DelVal = NewDelay;                      // initial delay
    Pntr0 = *DelayHead;                     // start from head
    Pntr1 = 0;

    // While not at end of list and a still positive delay.
    while (Pntr0 && DelVal > 0) {
        OldDelVal = DelVal;                 // save old interval
        DelVal -= task[Pntr0].delay;        // compute next
        // If interval is still positive
        if (DelVal > 0) {
            Pntr1 = Pntr0;                  //  step down the queue
            Pntr0 = task[Pntr1].next;
            }
        }

    // We are either at the end of the queue (<Pntr0> == 0) or
    // at the correct place in the queue to insert this task.
    task[NewTask].next = Pntr0;         // link this task in
    task[Pntr1].next = NewTask;
    if (Pntr0 == 0)
        task[NewTask].delay = DelVal;       // we are at the end
    else {
        if (Pntr0 == Pntr1)
            task[Pntr1].next = 0;
        task[NewTask].delay = OldDelVal;    // fix up us
        task[Pntr0].delay = -DelVal;        // and the next entry
        }
    task[NewTask].pntr = Tpntr;         // insert pointer

    NewTask = 0;
    NewDelay = 0;
}


//**********************************************************
// Run the task at <RunningTask>. When it returns clear the
// status and link. If it has re-scheduled itself, put it on
// the correct queue, otherwise clear the entry completely.

static void RunTask(void) {
   if(!(task[RunningTask].del)){ //del task
    (*task[RunningTask].pntr)();        // run the task
   }else {task[RunningTask].del=0;}
    task[RunningTask].next = 0;    //next task = 0
    // If the task has re-scheduled itself
    if (NewTask == RunningTask) {
        Tpntr = task[NewTask].pntr; // set task pointer
        // Put on correct list.
        if (NewDelay)
            DoQueueDelay();
        else
            DoQueueTask();
        }
    else
        task[RunningTask].pntr = 0; // ensure task is idle

    RunningTask = 0;
}


//**********************************************************
// Find either the first free entry in the data structure.
// Returns TRUE if a free spot could not be found.
// ���� ����� ��������� ���� � ���������
// ���������� 1 ���� ��������� �� ������ ��� ����� ������ ��� ����������
static unsigned int GetNewTask(void (*pntr)()) {
    
    NewTask = 1; // ������ � 1? ���������� 0 ��������� �� ����������
    while (NewTask <= TOTALTASKS) {
        if (task[NewTask].pntr == 0)
            return 0;
        if (task[NewTask].pntr == pntr)
            return 1;
        NewTask++;
        }
    return 1;
}

//******************************************************
// ���� � ������� ��� ������ ���������� ��� ��������
// �������� ������, �������� ��������� ������ � ����� ����������� �� ��������� �� �������.
volatile void DelTask(void (*TaskPtr)())
{
   for(unsigned char x=1;x!=(sizeof(task)/sizeof(task[0]));x++)
   {
      if(task[x].pntr == TaskPtr) {task[x].del=1;}
   }
}

//*****************************************************
//���� ������� Task � �������, ���������� �� ���������
// ���� ���������, ��� ��� � ������� ����� ���� ������ ���� Task
volatile unsigned char SearchTask(void (*TaskPtr)())
{
   unsigned char tasks=0;
   for(unsigned char x=1;x!=(sizeof(task)/sizeof(task[0]));x++)
   {
      if(task[x].pntr == TaskPtr) {tasks++;}
   }
   return tasks;
}

//******************************************************
// ���������� ������� �������� ������ ��� � ������� �����
volatile unsigned char FreeSizeQueue(void)
{
  unsigned char freeQueue = 0;
   for(unsigned char x=1;x!=(sizeof(task)/sizeof(task[0]));x++)
   {
      if(!(task[x].pntr)) freeQueue++;
   }
   return freeQueue;
}