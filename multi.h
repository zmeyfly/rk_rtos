// multi.h

#define	TOTALTASKS	10		// set for your application

extern void InitMulti(void);
extern unsigned int QueueTask(void (*TaskPtr)());
extern unsigned int QueueDelay(void (*TaskPtr)(), unsigned int Ticks);
extern void DecrementDelay(void);
extern void Dispatch(void);
extern void ReRunMe(unsigned int TaskNumber);
volatile void DelTask(void (*TaskPtr)());
volatile unsigned char SearchTask(void (*TaskPtr)());
volatile unsigned char FreeSizeQueue(void);
